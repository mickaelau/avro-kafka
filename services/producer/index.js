const Koa = require('koa');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const {
  SchemaRegistry,
  readAVSCAsync,
} = require('@kafkajs/confluent-schema-registry');

const { kafkaClient, TOPIC_NAME } = require('../kafka');

const registry = new SchemaRegistry({ host: 'http://schema-registry:8081/' });

const app = new Koa();

const producer = kafkaClient.producer();

const PORT = 3000;
const INIT_MESSAGE = `Running producer on port ${PORT}.`;

router.post('/produce', async (ctx) => {
  try {
    await producer.connect();

    const schema = await readAVSCAsync('../schema-registry/schemas/movie.avsc');
    const registryId = await registry.getRegistryIdBySchema(schema.name, schema);

    const data = await registry.encode(registryId, ctx.request.body);

    const response = await producer.send({
      topic: TOPIC_NAME,
      messages: [
        {
          key: 'mykey',
          value: data,
        },
      ],
    });

    ctx.body = response;
  } catch (error) {
    console.log(error.message);
    ctx.throw(500, {
      error: error.message,
    });
  }
});

app.use(bodyParser());
app.use(router.routes());

app.listen(PORT, async () => {
  console.log(INIT_MESSAGE);
});
