const { Kafka } = require('kafkajs');

const { KAFKA_USERNAME: username, KAFKA_PASSWORD: password } = process.env;
const sasl = username && password ? { username, password, mechanism: 'plain' } : null;
const ssl = !!sasl;

const kafkaClient = new Kafka({
  clientId: 'avro-kafka',
  brokers: [process.env.KAFKA_BOOTSTRAP_SERVER],
  ssl,
  sasl,
});

const TOPIC_NAME = 'movie-topic';

module.exports = {
  kafkaClient,
  TOPIC_NAME,
};
