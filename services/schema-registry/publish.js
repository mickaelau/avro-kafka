const {
  SchemaRegistry,
  readAVSCAsync,
} = require("@kafkajs/confluent-schema-registry");
const fs = require("fs");

const COMPATIBILITY = "FORWARD";

const base = "./services/schema-registry/schemas";
const registry = new SchemaRegistry({ host: "http://localhost:8081/" });

const publishSchema = async (schemaPath) => {
  const schema = await readAVSCAsync(schemaPath);
  await registry.register(schema, {
    compatibility: COMPATIBILITY,
    subject: schema.name,
  });
};

const publish = async () => {
  const filenames = fs
    .readdirSync(base)
    .map((filename) => `${base}/${filename}`);

  await Promise.all(filenames.map((filename) => publishSchema(filename)));
};

publish().catch(console.log);
