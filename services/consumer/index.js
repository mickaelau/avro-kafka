const Koa = require('koa');

const { SchemaRegistry } = require('@kafkajs/confluent-schema-registry');

const { kafkaClient, TOPIC_NAME } = require('../kafka');

const registry = new SchemaRegistry({ host: 'http://schema-registry:8081/' });

const app = new Koa();

const consumer = kafkaClient.consumer({ groupId: process.env.GROUP_ID });

const PORT = 3000;
const INIT_MESSAGE = `Running consumer on port ${PORT}.`;

const downstreamProcess = (data) => {
  // Perform some risky unethical access
  console.log(`${data.cast.length} actors are on the cast`);
};

const consume = async () => {
  await consumer.connect();
  await consumer.subscribe({
    topic: TOPIC_NAME,
    fromBeginning: true,
  });

  await consumer.run({
    eachMessage: async ({ message }) => {
      const data = await registry.decode(message.value);
      console.log('Received: ', data);

      downstreamProcess(data);
    },
  });
};

app.listen(PORT, async () => {
  console.log(INIT_MESSAGE);
  await consume();
});
